from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render

def index(request):
    name='Andrew'
    context={
        'name': name,
        'npm':1906350692
    }
    return render(request,"index.html",context)

def lab1(request):
    return render(request, 'lab1.html')

def lab2(request):
    return HttpResponseRedirect("https://www.figma.com/file/qpcVQ1Ebd16dSFTj8qlYNb/Story-2")

def challengelab2(request):
    return HttpResponseRedirect("https://www.figma.com/file/xOTlPBM3NnI9SxfHVkZpIb/Challenge-Story-2?node-id=0%3A1")

def lab3(request):
    return render(request,'lab3.html')

def lab3minigame(request):
    return render(request,'lab3-minigame.html')

