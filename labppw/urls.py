"""labppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import index,lab1,lab2,challengelab2,lab3, lab3minigame

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',index),
    path('lab1/',lab1),
    path('lab2/',lab2),
    path('lab2/challenge/',challengelab2),
    path('lab3/',lab3),
    path('lab3/minigame/',lab3minigame)
]
